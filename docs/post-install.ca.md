# Instruccions post-instal·lació DD

### Accessos locals login:

* MOODLE: https://moodle.DOMINI/login/index.php?saml=off

* NEXTCLOUD: https://nextcloud.DOMINI/login?direct=1

* WORDPRESS: https://wp.DOMINI/wp-login.php?normal




## 1. Instal·lació des de Zero:

- Clonar el directori de digital democratic
```bash
mkdir /opt/src
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic /opt/src/digitaldemocratic
cd /opt/src/digitaldemocratic
cp digitaldemocratic.conf.sample digitaldemocratic.conf

#variables aleatories de config
bash securize_conf.sh

#copiem directori custom
cp -r custom.sample custom


```


- Canviem el nom del domini i si omplim les variables de LETSENCRYPT ja genera els certificats
```bash
TITLE="Digital Democratic"
TITLE_SHORT="dd"
DOMAIN=digitaldemocratic.net
LETSENCRYPT_DNS=digitaldemocratic.net
LETSENCRYPT_EMAIL=suport-baixeras@digitaldemocratic.net
# Generate letsencrypt certificate for root domain
# Values:
# - false (default): dont generate certificate for root domain, only for                                                                  
#                    subdomains.
# - true: generate certificate for root domain and subdomains.
LETSENCRYPT_DOMAIN_ROOT=true

```

- repo update y dd-ctl all per arrencar
```

#update del repo i subrepos la primera vegada
./dd-ctl repo-update

#per arrencar
./dd-ctl all

```


## 2. Configuració:

### 2.0. Crear primer usuari a saml per poder fer proves

- anar a admin.DOMAIN
- crear un grup, per exemple: "docents"
- clic al botó de administration Resync
- anar a groups i verificar que apareix
- anar a users i crear l'usuari "docent01" del grup "docents" amb role "teacher"

### 2.1. Configuració del Keycloak

Go to https://sso.DOMINI/auth/admin/master/console

**THEMES**:

- [ ] login theme: liiibrelite
- [ ] account theme: account-avatar
- [ ] internazionalization enabled: ON
- [ ] default locale: ca



1. Configure -> Realm Settings -> Themes

Configurem d'aquest manera:

![](img/snapshot/1FGGqna.png)

**SECURITY DEFENSES**:

- [ ] Canviar segona línia de Content-Security-Policy per:
`frame-src 'self'; frame-ancestors 'self' *.DOMAIN localhost; object-src 'none';`

- [ ] La última per:
`max-age=31536000; includeSubDomains`

- [ ] Save

![](img/snapshot/uS5uqJB.png)

**CLIENT SCOPES**:

- [ ] client scopes => mappers => role_list => Single Role Attribute: ON

![](img/snapshot/Q2i349B.png)

![](img/snapshot/KYbY4ao.png)

![](img/snapshot/oJJPRdp.png)

**CLIENT**:

- [ ] Clients -> Account-console -> Settings -> Afegir a *Valid Redirect URIs* "https://moodle.DOMINI.net/*" a més de la de wp "https://wp.DOMINI.net/*"

![](img/snapshot/vgamSuC.png)

**EVENTS**:

![](img/snapshot/events-keycloak.png)


**CLIENTS / account**:

Afegeix URI de redirecció vàlids

`https://moodle.DOMINIDELCENTRE/*`
`https://wp.DOMINIDELCENTRE/*`
`/realms/master/account/*`
`https://nextcloud.DOMINIDELCENTRE/*`

![](img/snapshot/N_42e!m$3Fe.png)

### 2.2. Configuració Wordpress

![](img/snapshot/Nk8YPCI.png)

![](img/snapshot/3ZRPyzd.png)

Configurar el nickname de Wordpress:
![](img/snapshot/uOwYjOJ.png)

Script: 
```
var Output = user.getFirstName()+" "+user.getLastName();
Output;
```

**Per a que et permeti tancar sessió de SAML des de Wordpress:**

![](img/snapshot/myofFZv.png)

Afegim aquests paràmetres:

`/realms/master/account/*`
`https://wp.DOMAIN/*`

![](img/snapshot/7U9t8Zn.png)

Guardem la configuració.







### 2.3. Configuració Nextcloud

- Per configurar el email: 

![](img/snapshot/5jIt2EE.png)
![](img/snapshot/gMQAKmb.png)


**- Cercles:**

1. Per descarregar els Cercles: Aplicacions -> Aplicacions destacades -> Circles (Descarrega i activa)

![](img/snapshot/yyNyUvc.png)

2. Ara sortirà un menú nou

![](img/snapshot/IbRuJqC.png)

3. Tornem a la pantalla de paràmetres i anem a la secció de Administració -> "Treball en grup" o "Groupware":

![](img/snapshot/yjbOrLz.png)

O bé per linies de comandes:

```
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles members_limit --value="150"
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles allow_linked_groups --value="1"
docker exec -u www-data isard-apps-nextcloud-app php occ --no-warnings config:app:set circles skip_invitation_to_closed_circles --value="1
```

4. Afegir la red de docker com a whitelist. Administració -> Seguretat
![](img/snapshot/9RxNQNx.png)


### Neteja de caché del keycloak

Fer les comandes **una a una**:

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=false)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=false)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=-1)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheThemes,value=true)'`

`docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=cacheTemplates,value=true)' `

` docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='/subsystem=keycloak-server/theme=defaults/:write-attribute(name=staticMaxAge,value=2592000)'` 

` docker exec -ti isard-sso-keycloak /opt/jboss/keycloak/bin/jboss-cli.sh --connect --command='reload' `

## 3. Moodle Post-Install (Personalització)

### 3.1 Executar la següent ordre (ULL! abans cal emplenar els camps)

` php theme/cbe/postinstall.php --wwwroot=moodle.test1.digitaldemocratic.net --ncadmin=admin --ncpass=1234 --sitename="Digital Democratic" --contactname="Contact Name" --contactemail="contact@test.xxx" `

    --wwwroot=<wwwroot>  www Root.
    --ncadmin=<ncadmin>  Name Admin NextCloud.
    --ncpass=<ncpass>  Password Admin NextCloud.
    --sitename=<sitename>  Moodle Site Name.
    --contactname=<contactname>  Contact Name.
    --contactemail=<contactemail>  Contact Email.

### 3.1 Fer login com a admin de Moodle a: 

* url: https://moodle.DOMINI/login/index.php?saml=off
* Anar a: Administració del lloc/Notificacions

*En aquesta imatge pots afegir el nom del lloc, i altres paràmetres.*

![](img/snapshot/zzvSxW7.png)

### 3.2 Repositori NextCloud

1. Dins de NextCloud cal crear un client

![](img/snapshot/3ICWP5X.png)

- Name: moodle
- URI: https://moodle.test1.digitaldemocratic.net/admin/oauth2callback.php

Es crea un **Id Client** i un **Secret** que cal afegir en el oAuth2 de Moodle.


2. Crear servici oAuth2 a Moodle

https://moodle.test1.digitaldemocratic.net/admin/tool/oauth2/issuers.php

Crear nou servici NextCloud

![](img/snapshot/mkM8JN1.png)

Configurar amb aquestes dades:

- Name: NextCloud
- Client Id: **Id Client**
- Client Secret: **Secret**
- [OK] Autenticar solicituts de token a través de encapçalats HTTP
- URL base de servici: https://nextcloud.test1.digitaldemocratic.net

![](img/snapshot/KBV5ys2.png)

Per provar que funciona donem a la següent icona
![](img/snapshot/XLQNA9i.png)

I seguim els passos d´autenticació que ens marca NextCloud. Si apareix el Tic verd, estaria ben configurat.

3. Cal anar a 'Manage repositories' https://moodle.test1.digitaldemocratic.net/admin/repository.php

Activar i posar visible

Anar a Settings del Repositori NextCloud

![](img/snapshot/JGRbAJF.png)

Activar les dues opcions i salvar

![](img/snapshot/buRSMwg.png)

Crear una instància del Repositori

- Name: NextCloud
- Issuer: Seleccionem l'oAuth2 que hem creat anteriorment
- Folder: ''
- Supported files: Internal y External
- Return typ: Internal

[DESAR CANVIS]

3. Configurar plantilles OnlyOffice a Nextcloud

![](img/snapshot/ogGM_pzr3ybW.png)
Donar a desar

4. Carregar plantilles (opcional)
![](img/snapshot/Y!-rq;7GxjTW.png)


## 4. SAML PLUGINS ACTIVATION


### 4.1. Plugin de Wordpress SAML2


**1. Entrar com admin al WordPress (has d'estar amb la sessió tancada als altres entorns.):  https://wp.\<domain\>/wp-login.php?normal**

**2. Activar el plugin "OneLogin SAML SSO" i aplicar els canvis**





### 4.2. Configuració de WordPress



Verificar que el plugin GenerateBlock i el tema GeneratePress estan instal·lats i activats. 

![](img/snapshot/gZGNZXY.png)

![](img/snapshot/iThTdIa.png)


- Per configurar la hora i l'idioma

![](img/snapshot/JbyHUqJ.png)







## Troubleshooting

## -1. Instal·lació de software:

Per generar certificats multidomini i del domini principal:
```
apt install rsync vim tmux certbot -y
DOMAIN=digitaldemocratic.net
certbot certonly --preferred-challenges dns --manual --email digitaldemocratic@$DOMAIN --agree-tos -d *.$DOMAIN,$DOMAIN

```

Donar d'alta al dns el wildcard o els subdominis:
 - moodle
 - nextcloud
 - wp
 - oof
 - sso
 - pad
 - admin



## 0. Esborrar dades i/o configs

Si volem començar des de cero podem esborrar les dades i el repositori de codi (opcional)

Esborrar dades:
```
./dd-ctl reset-1714

```

Esborrar dades, configs, codi i certificats:

```
cd /opt/digitaldemocratic/src
./dd-ctl reset-1714
cd ..
rm -rf /opt/digitaldemocratic/src

hostname=test1
cp /opt/src/digitaldemocratic/digitaldemocratic.conf /opt/src/digitaldemocratic.conf.backup

git clone https://gitlab.com/digitaldemocratic/digitaldemocratic /opt/src/digitaldemocratic
cd /opt/src/digitaldemocratic
cp digitaldemocratic.conf.sample digitaldemocratic.conf
cp -r custom.sample custom
./securize_conf.sh
# Canvia els noms de domini de la configuració del dd pel hostname de la màquina
sed -i "s/DOMAIN=mydomain.com/DOMAIN=$hostname.digitaldemocratic.net/g" digitaldemocratic.conf
sed -i "s/LETSENCRYPT_DNS=/LETSENCRYPT_DNS=$hostname.digitaldemocratic.net/g" digitaldemocratic.conf
sed -i "s/LETSENCRYPT_EMAIL=/LETSENCRYPT_EMAIL=info@digitaldemocratic.net/g" digitaldemocratic.conf

./dd-ctl repo-update
```

### 2. Problemas con el dns si no va la renovación automática, hacerlo por challenge del dns

```
docker exec -ti isard-sso-haproxy /bin/sh
```

y dentro del docker:

```
mkdir /certs/selfsigned
mv /certs/*.pem /certs/selfsigned/
cat /etc/letsencrypt/live/$DOMAIN/fullchain.pem /etc/letsencrypt/live/$DOMAIN/privkey.pem > /certs/chain.pem
exit

```





