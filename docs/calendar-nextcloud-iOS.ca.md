# Manual Calendari Nextcloud per iOS

1. Entrar a Seguretat -> "Crea una nova contrasenya d'aplicació" (https://nextcloud.DOMINI/settings/user/security)

![](img/calendar-nextcloud-ios/manual_ios1.png)



**Es crea una contrasenya molt llarga i s'ha de guardar per fer-la servir després.**

2. Desde l'iOS, entrar al navegador amb l'adreça (https://nextcloud.DOMINI/remote.php/dav/principals/users/NOM_USUARI/), on posa NOM_USUARI és el vostre usuari de nextcloud i sortirà una pàgina on s'ha de posar el nom d'usuari i contrasenya que s'ha generat abans a la web.

Si s'ha fet bé es redirigeix a una pàgina com aquesta:

![](img/calendar-nextcloud-ios/manual_ios2.png)



3. Ajustos del dispositiu iOS -> Calendari -> Comptes -> Afegir compte -> Altre -> Afegir compte CalDAV

![](img/calendar-nextcloud-ios/manual_ios3.png)



- Server: Va l'adreça amb el nom d'usuari https://nextcloud.DOMINI/remote.php/dav/principals/users/NOM_USUARI/
- Username: el nom d'usuari
- Password: la contrasenya generada d'abans
- Description: el nom que es vulgui donar (per defecte posarà el del server)

Llavors es fa clic a "Advanced Settings" i s'ha d'habilitar el botó de "Use SSL" (el reste de dades s'omplen soles, sinò, es posa 443 al port i el mateix URL d'abans)

![](img/calendar-nextcloud-ios/manual_ios4.png)



I en principi ja està, es sincronitza directament. Si no s'afegeix al teu calendari directament:

Calendari (Aplicació) -> i afegir el compte del nextcloud que s'acaba de configurar.
