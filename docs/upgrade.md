
# Upgrade 20210621

To upgrade from previous versions:

1. Do a git pull in root folder, isard-apps and isard-sso and checkout master branch
2. Copy your existing keys in digitaldemocratic.conf to digitaldemocratic.conf.sample and use this new file as digitaldemocratic.conf
3. Comment the Makefile line that does the git submodule update
4. Execute make build
5. Execute de moodle script below to upgrade the new moodle plugins
6. Execute de wordpress script below to upgrade wordpress plugins
7. docker-compose up -d

## Upgrade moodle plugins
mkdir -p moodle/mod
mkdir -p moodle/mod/assign/submission
mkdir -p moodle/auth/saml2
mkdir -p moodle/theme/cbe
mkdir -p moodle/blocks
    curl --location https://moodle.org/plugins/download.php/23360/mod_jitsi_moodle310_2021052500.zip > jitsi.zip
    unzip jitsi.zip -d ./moodle/mod/
    rm jitsi.zip

    curl --location https://moodle.org/plugins/download.php/23294/mod_bigbluebuttonbn_moodle310_2019101009.zip > bbb.zip
    unzip bbb.zip -d ./moodle/mod/
    rm bbb.zip

    #ADD plugins/auth_saml2.zip ./moodle/
    curl --location https://github.com/isard-vdi/moodle-auth_saml2/archive/refs/heads/role_map.zip > auth_saml2.zip
    unzip auth_saml2.zip -d ./moodle/auth/
    mv ./moodle/auth/moodle-auth_saml2-role_map ./moodle/auth/saml2
    rm auth_saml2.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntshare/archive/refs/heads/master.zip > tresipuntshare.zip
    unzip tresipuntshare.zip -d ./moodle/mod/
    mv ./moodle/mod/moodle_mod_tresipuntshare-master ./moodle/mod/tresipuntshare
    rm tresipuntshare.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntvideo/archive/refs/heads/master.zip > tresipuntvideo.zip
    unzip tresipuntvideo.zip -d ./moodle/mod/
    mv ./moodle/mod/moodle_mod_tresipuntvideo-master ./moodle/mod/tresipuntvideo
    rm tresipuntvideo.zip

    curl --location https://github.com/3iPunt/moodle_mod_tresipuntaudio/archive/refs/heads/master.zip > tresipuntaudio.zip
    unzip tresipuntaudio.zip -d ./moodle/mod/
    mv ./moodle/mod/moodle_mod_tresipuntaudio-master ./moodle/mod/tresipuntaudio
    rm tresipuntaudio.zip

    curl --location https://github.com/3iPunt/moodle_assignsubmission_tipnc/archive/refs/heads/master.zip > assignsubmission_tipnc.zip
    mkdir -p ./moodle/mod/assign/submission/
    unzip assignsubmission_tipnc.zip -d ./moodle/mod/assign/submission/
    mv ./moodle/mod/assign/submission/moodle_assignsubmission_tipnc-master ./moodle/mod/assign/submission/tipnc
    rm assignsubmission_tipnc.zip

    # This blocks plugin would be better in its own repo
    curl --location https://gitlab.com/digitaldemocratic/digitaldemocratic/-/raw/master/custom.sample/moodle/plugins/block_tresipuntmodspend.zip?inline=false > block_tresipuntmodspend.zip
    unzip block_tresipuntmodspend.zip -d ./moodle/blocks/
    ##should be already here, no need to move path ./moodle/mod/tresipuntmodspend
    rm block_tresipuntmodspend.zip

    curl --location https://github.com/3iPunt/moodle_theme_cbe/archive/refs/heads/master.zip > tresipunt_theme_cbe.zip
    unzip tresipunt_theme_cbe.zip -d ./moodle/theme/
    mv ./moodle/theme/moodle_theme_cbe-master ./moodle/theme/cbe
    rm tresipunt_theme_cbe.zip
cp -R moodle/* /opt/digitaldemocratic/src/moodle/

##### Now upgrade from web ui with admin rights

# Upgrade wordpress mu-plugins
git clone https://gitlab.com/muplugins-multiste1/muplugins-google-sites.git
cp -r muplugins-google-sites/mu-plugins /opt/digitaldemocratic/src/wordpress/wp-content/mu-plugins/