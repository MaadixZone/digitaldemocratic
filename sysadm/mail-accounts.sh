#!/bin/bash -e
# bash due ${email:0:64}

# Script to create mailu email accounts from keycloak users and configure nextcloud email app

if [ -z "$1" -o -z "$2" -o -z "$3" ]
then
  echo "Usage: $0 <dd-server> <email-domain> <mailu-server>" >&2
  exit 1
fi

DD_SERVER=$1
EMAIL_DOMAIN=$2
MAILU_SERVER=$3

PASSWORD_FILE="$EMAIL_DOMAIN.passwords.$(date -Is)"
ssh root@$DD_SERVER "docker exec isard-apps-postgresql psql -U admin -d keycloak -tA -c 'select username from user_entity'" | while read user
do
  email="$user@$EMAIL_DOMAIN"
  if ! ssh -nT root@$MAILU_SERVER "cd /mailu && docker-compose -p mailu exec -T -u postgres database psql -d mailu -tA -c \"select email from public.user where email = '$user@$EMAIL_DOMAIN';\"" | grep -q "^$user@$EMAIL_DOMAIN$"
  then
    pass=$(diceware)
    if ssh -nT root@$DD_SERVER "docker exec -u www-data isard-apps-nextcloud-app php occ mail:account:create $user $user ${email:0:64} $MAILU_SERVER 993 ssl $email $pass $MAILU_SERVER 587 tls $email $pass"
    then
      if ssh -nT root@$MAILU_SERVER "cd /mailu && docker-compose -p mailu exec -T admin flask mailu user $user $EMAIL_DOMAIN $pass"
      then
        echo "$email $pass" >> $PASSWORD_FILE
      else
        echo "Error creating email account $email" >&2
        exit 1
      fi
    fi
  fi
done
