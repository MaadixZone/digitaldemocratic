#!/bin/sh -eu

cd "$(dirname "$0")/.."
./dd-ctl prerequisites

cat >&2 <<EOF

This script will be removed!
Please run './dd-ctl prerequisites' in the future.
EOF
