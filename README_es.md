[English](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_en.md) - [Català](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README.md) - [Castellano](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_es.md)

DD es el workspace educativo generado en el marco del Plan de Digitalitzación Democrática de Xnet. Ha sido creado y powered por Xnet, familias y centros promotores, IsardVDI, 3iPunt, Dirección de Innovación Democrática, Dirección de Innovación Digital, Comisionado de Economía Social del Ayuntamiento de Barcelona y Consorci d’Educació de Barcelona. En colaboración con aFFaC, Escola Montseny, Escola Baixeras, Maadix, AirVPN y Wemcor. Con la ayuda de Miriam Carles, Cristian Ruiz, Anna Francàs, Christopher Millard.

### Licencia

DD es una aplicación desarrollada como parte del Plan de Digitalización Democrática de Xnet y familias promotoras, con el apoyo de las Asociaciones Federadas de Familias de Alumnos de Cataluña (aFFaC).

La aplicación DD ha sido realizada por Xnet, IsardVDI y 3iPunt como parte de un proyecto piloto del Plan de Digitalización Democrática de Xnet financiado por la Dirección de Innovación Democrática, el Comisionado de Innovación Digital del Ayuntamiento de Barcelona y el Consorcio de Educación de Barcelona, al que han contribuido los centros participantes.

La aplicación DD puede utilizarse libremente siempre y cuando conste este footer y se respete la licencia AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.en.html).


### Créditos

Proyecto piloto del Plan de Digitalización Democrática dirigido por Xnet y familias promotoras. Programario creado por IsardVDI y 3iPunt con la colaboración de MaadiX.net, Affac, el Ayuntamiento de Barcelona y el Consorcio de Educación de Barcelona.

# Estado del proyecto

Proyecto en desarrollo. Estamos acabando el proyecto para en breves ponerlo a disposición de la comunidad para que podáis hacer las aportaciones y colaboraciones que deseáis.
En los próximos meses modificaremos el código con importantes cambios y mejoras. Una vez esté en un estado más estable lo publicaremos aquí mismo. 

# Qué es esto

Este proyecto permite facilitar un proveedor de identidad completo y muchas aplicaciones para tener un entorno pensado para escuelas y universidades. El proyecto proporcionará una solución integrada para gestionar el entorno común en la educación:

- **Aulas**: Una instancia de Moodle con tema personalizado y conectores personalizados.
- **Archivos**: Una instancia del Nextcloud con tema personalizado y conectores personalizados.
- **Documentos**: Una instancia de OnlyOffice integrada con Nextcloud.
- **Págines web**: Una instancia de Wordpress con el tema personalizado y conectores personalitzados.
- **Pad**: Una instancia Etherpad integrada con Nextcloud.
- **Conferencias**: Un BigBlueButton integrado con Moodle y Nextcloud. Necesita un servidor independiente.
- **Formularios**: Los conectores del Nextcloud de los formularios.
- ... (algunas aplicaciones como Jitsi o BigBlueButton no estan totalmente integradas ahora mismo)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Interfaz de administración

Ahora hay una interfaz de administración que permite gestionar fácilmente usuarios y grupos y mantenerlos sincronizados entre aplicaciones. Esto se hace ejecutando acciones sobre las apis de las diferentes aplicaciones.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

Para migrar e introducir fácilmente usuarios y grupos al sistema también hay dos importaciones:

- Desde la consola de administración de Google en formato JSON
- Desde un archivo CSV

Esta interfaz de administración está en estado alfa, pero ya permite gestionar usuarios sincronizados entre Keycloak, Moodle y Nextcloud.

## Requerimientos

Distribución de linux, **docker-ce**, and **docker-compose > 1.28**

```
docker-compose --version
# if version < 1.28: 
pip3 install --upgrade docker-compose
docker-compose --version
```

Si utilizas una distribución **debian**, para instalar las dependencias, docker y docker-compose puedes seguir: [sysadm/debian_docker_and_compose.sh](sysadm/debian_docker_and_compose.sh)

## Inicio rápido

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Cambia las contraseñas por defecto

```
./securize_conf.sh
```

Edita las variables del archivo digitaldemocratic.conf para satisfacer vuestras necesidades. 

```
cp -R custom.sample custom
```

Edita y sustituye los archivos para personalizar el sistema. 

La primera vez ejecuta:

```
./dd-ctl repo-update
```

Y después:

```
./dd-ctl all
```

NOTA: La autenticación SAML actualmente se encuentra automatizada:

- Moodle: No completamente automatitzado.
  1. Inicia la sesión en Moodle como administrador via: https://moodle.\<domain\>/login/index.php?saml=off  
  2. Ve a la configuración de autenticación: https://moodle.\<domain\>/admin/settings.php?section=manageauths
  3. Activa SAML2 haciendo clic al ojo.
  4. Clic a *configuración* a SAML2
  5. Haz clic en el botón *Regenera el certificado* dentro del formulario. Después de esto, vuelve a la página de configuración de SAML2.
  6. Haz clic en el botón Bloquea el *certificado*.
  7. Al terminal ejecuta el script para autoconfigurar: acoplador exec isard-sso-admin python3 moodle_saml.py
  8. La última cosa es purgar la memoria caché de moodle: ]]femida l'script php-fpm7 del acoplador Exec, haz lo através de moodle ui]]

- Nextcloud: Automatizada. Después de acabar el *make all* debería estar listo. En caso de que falle referirse a isard-sso/docs.
- Wordpress: Automatizada. Después de acabar el *make all* debería estar listo. En caso de que falle referirse a isard-sso/docs.

## Instrucciones de post instalación

Podéis encontrar un manual paso a paso en: (https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Instalación extendida

Puedes iniciar este proyecto en cualquier servidor con docker & docker-compose (cualquier sistema operativo debería funcionar). Para instalar estos paquetes en vuestra distribución, consulta el funcionamiento de docker & docker-compose en la documentación oficial y en la carpeta sysadm hay algunos scripts de automatización.

Cualquier distribución debería funcionar pero, si queréis utilizar nuestros scripts sysadm para instalar docker & docker-compose, utiliza Debian Buster (10).

### Clonar los submódulos 

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Referios a la documentación oficial (https://docs.docker.com/engine/install/) o utiliza el script en la carpeta sysadm para Debian Buster (10).

### docker-compose

Referios a la documentación oficial (https://docs.docker.com/compose/install/) o utiliza el script en la carpeta sysadm para Debian Buster (10).

### Configuración

Copia digitaldemocratic.conf.sample a digitaldemocratic.conf y editar para satisfacer vuestras necesidades. Como mínimo (para desarrollar) tenéis que adaptar la variable de DOMINI a vuestro dominio raíz.

- PRODUCCIÓN: Necesitaréis un dns multidominio (o redirige los subdominios múltiples) a vuestra máquina de anfitrión.
- Desarrollo: Edita el fichero /etc/hosts y añade los subdominis para propósitos de pruebas locales.

#### Subdominios

- Keycloak: sso.<yourdomain.org>
- Admin: admin.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>
- (opcional) FreeIPA: ipa.<yourdomain.org>

### Personalización

Copia recursivamente la carpeta *custom.sample* a *custom* y edita los archivos yaml de personalización y menú y sustituye las imágenes. 

### Iniciar el proyecto

La primera vez (y si queréis actualizar a la última versión posteriormente) ejecuta:

```
./dd-ctl update-repo
```

Y después:

```
./dd-ctl all
```

Posteriormente puedes iniciar o parar con:

```
./dd-ctl down
./dd-ctl up
```

### Integración

Lee el archivo [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) en la carpeta isard-sso/docs para integrar todas las aplicaciones. Ahora el Nextcloud y el Wordpress se deberían de integrar automáticamente con el Keycloak.

Vease tambien las [Intrucciones post-instalación en Catalan](https://digitaldemocratic.gitlab.io/digitaldemocratic/post-install.ca/).
